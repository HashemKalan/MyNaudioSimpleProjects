﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio;
using NAudio.Wave;
namespace Mp3ToWaveConverter
{
    public partial class Form1 : Form
    {
        string mp3FileName = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a Mp3 File";
            op.Filter = "Mp3 File(*.mp3)|*.mp3";
            if (op.ShowDialog() == DialogResult.OK)
            {
                mp3FileName = op.FileName;
            }
        }
        private void Convert(string mp3FileName, string outPutFileName)
        {
            using (Mp3FileReader reader = new Mp3FileReader(mp3FileName))
            {
                WaveFileWriter.CreateWaveFile(outPutFileName, reader);
            }
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(mp3FileName))
            {
                try
                {
                    Convert(mp3FileName, Application.StartupPath + "\\" + System.IO.Path.GetFileNameWithoutExtension(mp3FileName) + ".wav");
                    MessageBox.Show("Convert Compeleted");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
