﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio;
using NAudio.Wave;
namespace Mp3Player
{
    public partial class Form1 : Form
    {
        IWavePlayer waveOutDevice;
        AudioFileReader audioFileReader;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = "Mp3File(*.mp3)|*.mp3";
            if (op.ShowDialog() == DialogResult.OK)
            {
                txtPath.Text = op.FileName;
                audioFileReader = new AudioFileReader(txtPath.Text);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            waveOutDevice = new WaveOut();
            if (!string.IsNullOrEmpty(txtPath.Text))
            {                
                waveOutDevice.Init(audioFileReader);
                waveOutDevice.Play();
                string time = audioFileReader.TotalTime.Minutes + ":" + audioFileReader.TotalTime.Seconds;
                lblStatus.Text = time;
            }
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            if (waveOutDevice != null)
            {
                waveOutDevice.Pause();
            }
        }

        private void trackBarVolume_Scroll(object sender, EventArgs e)
        {
            if (waveOutDevice != null)
            {                
                waveOutDevice.Volume = (float)((float)trackBarVolume.Value / 100);
            }
        }
    }
}
