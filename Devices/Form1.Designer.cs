﻿namespace Devices
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstInputDevices = new System.Windows.Forms.ListBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lstOutputDevices = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lstInputDevices
            // 
            this.lstInputDevices.FormattingEnabled = true;
            this.lstInputDevices.HorizontalScrollbar = true;
            this.lstInputDevices.Location = new System.Drawing.Point(13, 13);
            this.lstInputDevices.Name = "lstInputDevices";
            this.lstInputDevices.Size = new System.Drawing.Size(163, 238);
            this.lstInputDevices.TabIndex = 0;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(355, 13);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(132, 23);
            this.btnRefresh.TabIndex = 1;
            this.btnRefresh.Text = "Refresh Lists";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lstOutputDevices
            // 
            this.lstOutputDevices.FormattingEnabled = true;
            this.lstOutputDevices.HorizontalScrollbar = true;
            this.lstOutputDevices.Location = new System.Drawing.Point(186, 12);
            this.lstOutputDevices.Name = "lstOutputDevices";
            this.lstOutputDevices.Size = new System.Drawing.Size(163, 238);
            this.lstOutputDevices.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 262);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lstOutputDevices);
            this.Controls.Add(this.lstInputDevices);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstInputDevices;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ListBox lstOutputDevices;
    }
}

