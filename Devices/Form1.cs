﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio;
using NAudio.Wave;
namespace Devices
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            showInputDevices();
            showOutputDevices();
        }

        private void showOutputDevices()
        {
            lstOutputDevices.Items.Clear();
            int c = WaveOut.DeviceCount;
            for (int i = 0; i < c; i++)
            {
                WaveOutCapabilities info = WaveOut.GetCapabilities(i);
                lstOutputDevices.Items.Add(string.Format("{0},{1}", info.ProductName, info.Channels));
            }
        }

        private void showInputDevices()
        {
            lstInputDevices.Items.Clear();
            int c = WaveIn.DeviceCount;
            for (int i = 0; i < WaveIn.DeviceCount; i++)
            {
                WaveInCapabilities info = WaveIn.GetCapabilities(i);
                lstInputDevices.Items.Add(string.Format("{0},{1}", info.ProductName, info.Channels));
            }
        }
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Form1_Load(null, null);
        }
    }
}
