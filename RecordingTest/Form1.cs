﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio;
using NAudio.Wave;
namespace RecordingTest
{    
    public partial class Form1 : Form
    {
        public WaveIn waveSource = null;
        public WaveFileWriter waveFile = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnEnd.Enabled = true;
            waveSource = new WaveIn();
            waveSource.WaveFormat = new WaveFormat(44100, 1);
            waveSource.DataAvailable += WaveSource_DataAvailable;
            waveSource.RecordingStopped += WaveSource_RecordingStopped;
            waveFile = new WaveFileWriter(@"C:\a.wav", waveSource.WaveFormat);
            waveSource.StartRecording();
        }

        private void WaveSource_RecordingStopped(object sender, StoppedEventArgs e)
        {
            if(waveSource!=null)
            {
                waveSource.Dispose();
                waveSource = null;
            }
            if(waveFile!=null)
            {
                waveFile.Dispose();
                waveFile = null;
            }
        }

        private void WaveSource_DataAvailable(object sender, WaveInEventArgs e)
        {
            if(waveFile!=null)
            {                
                waveFile.Flush();
                waveFile.Write(e.Buffer, 0, e.BytesRecorded);
            }
        }

        private void btnEnd_Click(object sender, EventArgs e)
        {
            btnEnd.Enabled = false;
            btnStart.Enabled = true;
            waveSource.StopRecording();
        }
    }
}
